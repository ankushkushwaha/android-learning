package com.example.studentbase.controller;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.studentbase.R;
import com.example.studentbase.model.DatabaseController.DBChangeReceiver;
import com.example.studentbase.model.DatabaseController.DBHelper;
import com.example.studentbase.model.InternalModel.Student;
import com.example.studentbase.model.RemoteModel.User;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Ankush on 6/6/17.
 */

public class RemoteUserAdapter extends RecyclerView.Adapter<RemoteUserAdapter.ViewHolder>{

    private static final String TAG = "RemoteUserAdapter";

    private List<User> mDataSet;
    private Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView nameTextView;
        private TextView idTextView;
        private TextView emailTextView;

        private User user;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    public RemoteUserAdapter(List<User> dataSet, Context context) {
        this.mDataSet = dataSet;
        this.context = context;

    }


    @Override
    public RemoteUserAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.remote_user_item, viewGroup, false);

        return new RemoteUserAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        viewHolder.nameTextView = (TextView) viewHolder.itemView.findViewById(R.id.textView3);
        viewHolder.idTextView = (TextView) viewHolder.itemView.findViewById(R.id.textView4);
        viewHolder.emailTextView = (TextView) viewHolder.itemView.findViewById(R.id.textView5);

        viewHolder.user = mDataSet.get(position);

        viewHolder.nameTextView.setText(viewHolder.user.getId());
        viewHolder.idTextView.setText(viewHolder.user.getName());
        viewHolder.emailTextView.setText(viewHolder.user.getEmail());
    }

//    @Override
//    public int getItemCount() {
//        return 200;
//    }

        @Override
    public int getItemCount() {
        return mDataSet.size();
    }


}
