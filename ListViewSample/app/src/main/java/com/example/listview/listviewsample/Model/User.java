package com.example.listview.listviewsample.Model;

/**
 * Created by Ankush on 5/22/17.
 */

public class User {
    public String name;
    public String hometown;

    public User(String name, String hometown) {
        this.name = name;
        this.hometown = hometown;
    }
}
