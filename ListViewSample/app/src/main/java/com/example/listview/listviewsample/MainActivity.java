package com.example.listview.listviewsample;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.example.listview.listviewsample.Model.User;
import com.example.listview.listviewsample.Model.UsersAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


//        String[] values = new String[] { "Ankush", "Android", "iPhone",
//                "Windows", "Ubuntu", "Android", "iPhone", "Windows" };
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
//                android.R.layout.simple_list_item_2, values);
//
//        ListView l = (ListView) findViewById(R.id.list_view);
//
//        l.setAdapter(adapter);





//        ArrayList<Map<String, String>> list = buildData();
//        String[] from = { "name", "purpose" };
//        int[] to = { android.R.id.text1, android.R.id.text2 };
//
//        SimpleAdapter adapter = new SimpleAdapter(this, list,
//                android.R.layout.simple_list_item_activated_2, from, to);
//
//
//        ListView l = (ListView) findViewById(R.id.list_view);
//        l.setAdapter(adapter);






        // Construct the data source
        ArrayList<User> arrayOfUsers = new ArrayList<User>();
// Create the adapter to convert the array to views
        UsersAdapter adapter = new UsersAdapter(this, arrayOfUsers);
// Attach the adapter to a ListView
        ListView listView = (ListView) findViewById(R.id.list_view);
        listView.setAdapter(adapter);


        // Add item to adapter
        User newUser = new User("Nathan", "San Diego");
        adapter.add(newUser);

        User newUser2 = new User("Nathan2", "San Diego2");
        adapter.add(newUser2);

// Or even append an entire new collection
// Fetching some data, data has now returned
// If data was JSON, convert to ArrayList of User objects.
//        JSONArray jsonArray = ...;
//        ArrayList<User> newUsers = User.fromJson(jsonArray)
//        adapter.addAll(newUsers);

    }


//    private ArrayList<Map<String, String>> buildData() {
//        ArrayList<Map<String, String>> list = new ArrayList<Map<String, String>>();
//        list.add(putData("Android", "Mobile"));
//        list.add(putData("Windows7", "Windows7"));
//        list.add(putData("iPhone", "iPhone"));
//        return list;
//    }
//
//    private HashMap<String, String> putData(String name, String purpose) {
//        HashMap<String, String> item = new HashMap<String, String>();
//        item.put("name", name);
//        item.put("purpose", purpose);
//        return item;
//    }

}
