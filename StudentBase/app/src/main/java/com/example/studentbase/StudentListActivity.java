package com.example.studentbase;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;


import com.example.studentbase.controller.CustomAdapter;
import com.example.studentbase.model.DatabaseController.DBChangeReceiver;
import com.example.studentbase.model.DatabaseController.DBHelper;

import static android.R.id.list;

/**
 * Created by Ankush on 5/26/17.
 */

public class StudentListActivity extends Activity {

    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";

    public static final String TAG = "StudentListActivity";

    private RecyclerView.LayoutManager mLayoutManager;

    private CustomAdapter adapter;

    DBHelper dbHelper;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_activity);


        mLayoutManager = new LinearLayoutManager(this);

//        List<Student> arrayOfUsers = new ArrayList<Student>();
//        for(int l=0; l<=15; l++){
//            Student newUser = new Student("Nathan"+l, "description"+l, "roll number" + l);
//            arrayOfUsers.add(newUser);
//        }

        this.dbHelper = new DBHelper(this);

        this.adapter = new CustomAdapter(dbHelper.getAllStudents(), this.getApplicationContext());

        RecyclerView listView = (RecyclerView) findViewById(R.id.xxx);

        listView.setLayoutManager(mLayoutManager);

        if (listView == null) {
            Log.i(TAG, "ListView is null");

        }

        listView.setAdapter(adapter);
    }

    public void loadAddRecordScreen(View view) {
        Intent intent = new Intent(this, AddRecordActivity.class);
        String message = "hello dude";
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);

        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

    }

}
