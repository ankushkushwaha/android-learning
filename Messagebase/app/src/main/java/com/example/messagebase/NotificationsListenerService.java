package com.example.messagebase;

import android.os.Bundle;

import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ankush on 6/15/17.
 */

public class NotificationsListenerService extends GcmListenerService {


    @Override
    public void onMessageReceived(String from, Bundle data) {
        super.onMessageReceived(from, data);

        JSONParser parser = new JSONParser(); JSONObject json = (JSONObject) parser.parse(stringToParse);


        String myJSONString = data.get("notification").toString();

        JSONObject myJson = null;
        try {
            myJson = new JSONObject(myJSONString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String commentText = data.get("latestComment").toString();

        int registration_id = myJson.optInt("registration_id ");
        String time_to_live = myJson.optString("time_to_live");


//        String json = intent.getExtras().getString("payload");


    }
}
