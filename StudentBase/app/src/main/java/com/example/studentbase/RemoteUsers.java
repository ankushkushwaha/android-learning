package com.example.studentbase;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.studentbase.controller.CustomAdapter;
import com.example.studentbase.controller.RemoteUserAdapter;
import com.example.studentbase.controller.WebServices.UserService;
import com.example.studentbase.model.DatabaseController.DBHelper;
import com.example.studentbase.model.RemoteModel.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Ankush on 6/2/17.
 */

public class RemoteUsers extends Activity {

    private static final String TAG = "RemoteUsers";

    private RemoteUserAdapter adapter;
    private LinearLayoutManager mLayoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.remote_userce_activity);



        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        UserService service = retrofit.create(UserService.class);

        Call<List<User>> users = service.listUser("users");


        users.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse( Response<List<User>> response, Retrofit retrofit) {

                final Response<List<User>> result =  (Response<List<User>>) response;

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {

                        setupUI(result.body());

                        Log.d("UI thread", "I am the UI thread");
                    }
                });
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("fail:", "");
            }
        });
    }

    void setupUI(List<User> users)
    {
        this.mLayoutManager = new LinearLayoutManager(this);



        this.adapter = new RemoteUserAdapter(users, this);

        RecyclerView listView = (RecyclerView) findViewById(R.id.remote_user_recycler_view);

        listView.setLayoutManager(this.mLayoutManager);

        if (listView == null) {
            Log.i(TAG, "ListView is null");

        }

        listView.setAdapter(adapter);
    }

}
