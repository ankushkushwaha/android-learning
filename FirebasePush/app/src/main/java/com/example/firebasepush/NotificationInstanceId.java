package com.example.firebasepush;


import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Ankush on 6/15/17.
 */

public class NotificationInstanceId extends FirebaseInstanceIdService {

    private static final String TAG = NotificationInstanceId.class.getSimpleName();


    @Override
    public void onTokenRefresh() {

        Log.d("","token worked");

        String tkn = FirebaseInstanceId.getInstance().getToken();
        Log.d("Not","Token ["+tkn+"]");

    }
}
