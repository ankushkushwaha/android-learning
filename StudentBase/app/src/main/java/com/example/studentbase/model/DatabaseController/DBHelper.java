package com.example.studentbase.model.DatabaseController;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.studentbase.BuildConfig;
import com.example.studentbase.model.InternalModel.Student;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Ankush on 5/30/17.
 */

public class DBHelper extends SQLiteOpenHelper  {
    // If you change the database schema, you must increment the database version.
    public static final String TAG = "DBHelper";

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "StudentBase.db";

    private Context context;


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        this.context = context;
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(StudentContract.getCreateStudentsRecordQuery());
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(StudentContract.getDropTableQuery());
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public long insertStudentRecord (Student student) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(StudentContract.StudentEntry.COLUMN_NAME_NAME, student.name);
        values.put(StudentContract.StudentEntry.COLUMN_NAME_ROLLNUMBER, student.rollNumber);
        values.put(StudentContract.StudentEntry.COLUMN_NAME_CLASS, student.Class);
        values.put(StudentContract.StudentEntry.COLUMN_NAME_PERCENTAGE, student.percentage);

        long newRowId = db.insert(StudentContract.StudentEntry.TABLE_NAME, null, values);

        Log.d("Inserted ID: ", "newRowId="+newRowId);

        this.databseChanged();

        return newRowId;
    }

    public int deleteStudentForId (String studentID) {

        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = "_id=?";

        int affectedRow = db.delete(StudentContract.StudentEntry.TABLE_NAME, whereClause, new String[]{studentID} );

       if (affectedRow > 0) // returns number of row affected
       {
           this.databseChanged();

           Log.d(TAG, "Deleted ID:="+studentID);
       }
       else if (BuildConfig.DEBUG)
       {
           throw new RuntimeException(TAG+": This is a crash");
       }
       return affectedRow;

    }
    public List<Student> getAllStudents() {

        List<Student> studentList = new ArrayList<Student>();
        String selectQuery = "SELECT  * FROM " + StudentContract.StudentEntry.TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Student contact = new Student(
                        Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1),
                        Integer.parseInt(cursor.getString(2)),
                        Integer.parseInt(cursor.getString(3)),
                        Double.parseDouble(cursor.getString(4))
                );
                studentList.add(contact);
            } while (cursor.moveToNext());
        }

        return studentList;
    }

    public void databseChanged ()
    {
        Intent i = new Intent("com.studentbase.DATABASE_CHANGED");
        i.putExtra("KEY1","VALUE1");
        i.putExtra("KEY2","VALUE2");
        i.putExtra("KEY3","VALUE3");

        LocalBroadcastManager.getInstance(null).sendBroadcast(i);

    }
}
