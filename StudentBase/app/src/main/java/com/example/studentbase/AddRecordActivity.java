package com.example.studentbase;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.studentbase.model.DatabaseController.DBHelper;
import com.example.studentbase.model.InternalModel.Student;

/**
 * Created by Ankush on 5/29/17.
 */

public class AddRecordActivity extends Activity {

    public static final String TAG = "AddRecordActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.add_record_activity);

        Intent intent = getIntent();
        String message = intent.getStringExtra(StudentListActivity.EXTRA_MESSAGE);

        Log.d(TAG, message);

     }

    public void back(View view) {

        try
        {
            EditText roll = (EditText) findViewById(R.id.editText);
            EditText name = (EditText) findViewById(R.id.editText2);
            EditText Class = (EditText) findViewById(R.id.editText3);

            Student s = new Student(name.getText().toString(), Integer.parseInt(roll.getText().toString()), Integer.parseInt(roll.getText().toString()), 50.5);

            this.addStudent(s);

        }catch (Exception e) {

            Toast.makeText(this.getBaseContext(),"Could not add record",Toast.LENGTH_SHORT).show();

        }

        finish();

        this.overridePendingTransition(R.anim.push_up_in,R.anim.push_up_out);

    }

    private void addStudent(Student student){
        DBHelper dbHelper = new DBHelper(this);

        dbHelper.insertStudentRecord(student);
    }

}
