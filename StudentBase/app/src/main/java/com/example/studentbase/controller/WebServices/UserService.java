package com.example.studentbase.controller.WebServices;

import com.example.studentbase.model.RemoteModel.User;

import org.json.JSONObject;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;


/**
 * Created by Ankush on 6/2/17.
 */

public interface UserService {
    @GET("{users}")
    Call<List<User>> listUser(@Path("users") String user);

//    Call<User> userInfo(@Path("users") String user);

}