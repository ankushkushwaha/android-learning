package com.example.studentbase.model.DatabaseController;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.studentbase.StudentListActivity;
import com.example.studentbase.controller.CustomAdapter;

/**
 * Created by Ankush on 5/30/17.
 */

public class DBChangeReceiver extends BroadcastReceiver {
    public static String ACTION_DATABASE_CHANGED = "com.studentBase.DATABASE_CHANGED";

    public CustomAdapter adapterForStudentList;

    private static DBChangeReceiver instance = null;

    private Context context;

    //a private constructor so no instances can be made outside this class
    private DBChangeReceiver(Context context) {
        this.context = context;
    }

    //Everytime you need an instance, call this
    public static DBChangeReceiver sharedInstance(Context context) {

//        synchronized (instance) {

            if(instance == null) {
                instance = new DBChangeReceiver(context);

                LocalBroadcastManager.getInstance(instance.context).registerReceiver(instance,
                        new IntentFilter("com.studentbase.DATABASE_CHANGED"));
            }
            return instance;
//        }

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("receiver", "Got message: " );

        this.adapterForStudentList.DBChangedCallback();
    }

}