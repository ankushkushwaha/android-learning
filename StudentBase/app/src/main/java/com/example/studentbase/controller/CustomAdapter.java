/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.example.studentbase.controller;

import com.example.studentbase.R;
import com.example.studentbase.model.DatabaseController.DBChangeReceiver;
import com.example.studentbase.model.DatabaseController.DBHelper;
import com.example.studentbase.model.InternalModel.Student;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Provide views to RecyclerView with data from mDataSet.
 */
public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder> {
    private static final String TAG = "CustomAdapter";

    private List<Student> mDataSet;
    private Context context;

      public static class ViewHolder extends RecyclerView.ViewHolder {

          private TextView nameTextView;
          private TextView classTextView;
          private Button button;
          private ImageView imageView;

          private Student student;

        public ViewHolder(View v) {
            super(v);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.d(TAG, "Element " + getAdapterPosition() + " clicked.");
                }
            });

        }
    }

    public CustomAdapter(List<Student> dataSet, Context context) {
        this.mDataSet = dataSet;
        this.context = context;
        DBChangeReceiver.sharedInstance(context).adapterForStudentList = this;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.student_list_cell, viewGroup, false);

        return new ViewHolder(v);
}

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");

        viewHolder.nameTextView = (TextView) viewHolder.itemView.findViewById(R.id.textView);
        viewHolder.classTextView = (TextView) viewHolder.itemView.findViewById(R.id.textView2);
        viewHolder.button = (Button) viewHolder.itemView.findViewById(R.id.button4);
        viewHolder.imageView = (ImageView) viewHolder.itemView.findViewById(R.id.imageView);

        viewHolder.student = mDataSet.get(position);

        viewHolder.nameTextView.setText(viewHolder.student.name);
        viewHolder.classTextView.setText(viewHolder.student.Class);
        Picasso.with(context).load("http://placehold.it/500x500&text="+viewHolder.student.name)
                .into(viewHolder.imageView);

        viewHolder.button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                DBHelper db = new DBHelper(context);

                db.deleteStudentForId(Integer.toString( mDataSet.get(position).getID() ));
            }
        });

    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }



    public void  DBChangedCallback() {

        DBHelper dbHelper = new DBHelper(this.context);
        mDataSet.clear();
        mDataSet.addAll(dbHelper.getAllStudents());
        notifyDataSetChanged();
    }

}
