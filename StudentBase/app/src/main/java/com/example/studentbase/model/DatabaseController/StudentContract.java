package com.example.studentbase.model.DatabaseController;

import android.provider.BaseColumns;

/**
 * Created by Ankush on 5/30/17.
 */

public class StudentContract {

    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private StudentContract() {}

    /* Inner class that defines the table contents */
    public static class StudentEntry implements BaseColumns {

        public static final String TABLE_NAME = "student";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_ROLLNUMBER = "roll_number";
        public static final String COLUMN_NAME_CLASS = "class";
        public static final String COLUMN_NAME_PERCENTAGE = "percentage";

        private static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + StudentEntry.TABLE_NAME + " (" +
                        StudentEntry._ID + " INTEGER PRIMARY KEY," +
                        StudentEntry.COLUMN_NAME_NAME + " TEXT," +
                        StudentEntry.COLUMN_NAME_ROLLNUMBER + " INTEGER," +
                        StudentEntry.COLUMN_NAME_CLASS + " INTEGER," +
                        StudentEntry.COLUMN_NAME_PERCENTAGE + " DOUBLE)";

        private static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + StudentEntry.TABLE_NAME;

    }

    public static String getCreateStudentsRecordQuery(){
        return StudentEntry.SQL_CREATE_ENTRIES;
    }

    public static String getDropTableQuery(){
        return StudentEntry.SQL_DELETE_ENTRIES;
    }

}
