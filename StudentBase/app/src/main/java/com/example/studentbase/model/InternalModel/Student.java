package com.example.studentbase.model.InternalModel;


/**
 * Created by Ankush on 5/26/17.
 */

public class Student {

    private int ID;
    public String name;
    public String description;
    public String rollNumber;
    public String Class;
    public String percentage;

    public int getID(){
        return this.ID;
    }

    public Student (String name, String description, String rollNumber)
    {
        this.name = name;
        this.description = description;
        this.rollNumber = rollNumber;
    }

    public Student(String name, int rollNumber, int Class, Double percentage){
        this.name = name;
        this.rollNumber = ""+rollNumber;
        this.Class = Class+"";
        this.percentage = percentage.toString();

    }

    public Student(int id, String name, int rollNumber, int Class, Double percentage){
        this.ID = id;
        this.name = name;
        this.rollNumber = ""+rollNumber;
        this.Class = Class+"";
        this.percentage = percentage.toString();

    }
}
